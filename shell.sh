#!/bin/bash

# echo "Enter Source Folder:"
# read src

# echo "Enter Destination Folder"
# read dest

src=$1
dest=$2

#checking arguments
if [ $# -ne 2 ];
then
    echo "Pass Source and Destination as command line arguments $0 <source> <destintion>"
fi

#check if source folder exist
if [ ! -d "$src" ];
then
        echo "Source directory $src does not exist!"
        exit 1
fi

#check if source folder is empty
if [ -z "$(ls -A $src)" ];
then
        echo "$src directory is empty!"
        exit 1
fi

#check if dest dir exist, creating if not exist
if [ ! -d "$dest" ];
then
        mkdir -p "$dest"
fi

#checks if specific file or folder already exist in destination
for file in "$src"/*;
do
        base_file="$(basename $file)"
        if [ -e "$dest/$base_file" ];
        then
                echo "Warning $base_file already exist!"
        else
                cp -r "$file" "$dest"
                if [ $? == 0 ];
                then
                        echo "$base_file copied succesfully!"
                else
                        echo "Error occured while copying $base_file"
                fi
        fi
done
